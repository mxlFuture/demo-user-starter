package com.example.demouserstarter.config;


import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "user")
public class UserProperties {
    private String uname;
    private Integer age;
    private String address;

    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "UserProperties{" +
                "uname='" + uname + '\'' +
                ", age=" + age +
                ", address='" + address + '\'' +
                '}';
    }
}
