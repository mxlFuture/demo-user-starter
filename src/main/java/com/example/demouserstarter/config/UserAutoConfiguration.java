package com.example.demouserstarter.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;

@Configuration
@EnableConfigurationProperties(UserProperties.class)
public class UserAutoConfiguration {

    @Resource
    UserProperties userProperties;

    @Bean
    public UserService userService() {
        return new UserService(userProperties);
    }
}
