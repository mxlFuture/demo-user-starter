package com.example.demouserstarter.config;


public class UserService {
    private final UserProperties userProperties;

    public UserService(UserProperties userProperties) {
        this.userProperties = userProperties;
    }

    public void test() {
        System.out.println("User Name: " + userProperties.getUname());
        System.out.println("User Age: " + userProperties.getAge());
        System.out.println("User Address: " + userProperties.getAddress());
    }
}